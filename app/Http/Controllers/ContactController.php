<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Mail;

class ContactController extends Controller
{
    public function save(Request $request){
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'message' => 'required'
        ]);

        $contact = new Contact;

        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->email = $request->email;
        $contact->phone = $request->phone_number;
        $contact->message = $request->message;

        $contact->save();

       //email 
		\Mail::send('email',
             array(
                 'first_name' => $request->get('first_name'),
                 'last_name' => $request->get('last_name'),
                 'email' => $request->get('email'),
                 'phone' => $request->get('phone_number'),
                 'user_message' => $request->get('message'),
             ), function($message) use ($request)
               {
	              $subject = 'Email From '.$request->first_name.' '.$request->last_name;
                  $message->subject($subject);
                  $message->from(env('MAIL_USERNAME'));
                  $message->to(env('MAIL_USERNAME'));
               });

        return back()->with('success', 'Thank you for contact us!');
    }
}

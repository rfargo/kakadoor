<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<h2>Hello Admin,</h2>
You received an email from : {{ $first_name.' '.$last_name }}
<br/>
<br/>
Here are the details:
<br/>
<b>First Name:</b> {{ $first_name }}
<br/>
<b>Last Name:</b> {{ $last_name }}
<br/>
<b>Email:</b> {{ $email }}
<br/>
<b>Phone Number:</b> {{ $phone }}
<br/>
<b>Message:</b> {{ $user_message }}
<br/>
Thank You

</body>
</html>


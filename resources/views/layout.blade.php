<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
        <title>Kaka | @yield('title')</title>

        @include('meta::manager', [
            'description'   => 'KAKA Door menawarkan pintu yang berbeda dari produk serupa yang ada di pasaran baik dari penampilan, konstruksi, maupun bahan',
            'image'         =>  url('/img/KAKA DOOR - LOGO-01.png'),
            'keywords'      => 'pintu, pintu kamar, pintu kayu, kayu, pintu kamar kayu, pintu rumah, pintu rumah kayu, kamar, rumah, pintu minimalis, minimalis, pintu kamar minimalis, piutu rumah minimalis, pintu kayu minimalis modern, ',
            'geo_region'    => 'ID',
            'type'          => 'website',
            'robots'        => 'index, follow'
        ])

        <meta name="geo.region" content="ID" />

        <link rel="icon" href="{{ URL::asset('/img/KAKA DOOR - LOGO-01.png') }}" type="image/x-icon"/>

        <link rel="canonical" href="{{ url()->current() }}" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;300;400;600;700;900&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-W0BMPTZMQX"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-W0BMPTZMQX');
        </script>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

        <script async type="text/javascript">
        $( document ).ready(function() {
            var pathname = $(location).attr("pathname");

            if ($(window).width() > 1024){
                //desktop
                if (pathname == "/") {
                    $('.home-nav').addClass('href-nav');
                }
                if (pathname == "/about") {
                    $('.about-nav').addClass('href-nav');
                }
                if (pathname == "/product" || pathname == "/product/pintu" || pathname == "/product/kusen") {
                    $('.product-nav').addClass('href-nav');
                }
                if (pathname == "/contact") {
                    $('.contact-nav').addClass('href-nav');
                }
            }else{
                //mobile and tablet
                if (pathname == "/") {
                    $('.home-nav').addClass('href-nav');
                }
                if (pathname == "/about") {
                    $('.about-nav').addClass('href-nav');
                }
                if (pathname == "/product") {
                    $('.product-nav').addClass('href-nav');
                }
                if (pathname == "/product/pintu") {
                    $('.pintu-nav').addClass('href-nav');
                }
                if (pathname == "/product/kusen") {
                    $('.kusen-nav').addClass('href-nav');
                }
                if (pathname == "/contact") {
                    $('.contact-nav').addClass('href-nav');
                }
            }

            if ($(window).width() < 480) {
                //mobile
             $('.navbar').addClass('sticky-top');
             $('#drop-down').removeClass('dropdown-menu');
             $('#navbarDropdown').removeClass('dropdown-toggle');
            }
        });
        
        </script>

    </head>

    <body class="antialiased">
        <div class="relative items-top justify-center min-h-screen dark:bg-gray-900 sm:items-center sm:pt-0">

        <nav class="navbar navbar-expand-md navbar-light">
          <a class="navbar-brand" href="/">
            <img src="{{ asset('img/KAKA DOOR - LOGO-01.png') }}" alt="Logo KakaDoor">
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mt-2 mt-lg-0 ml-auto">
              <li class="nav-item">
                <a class="home-nav nav-link" href="/">Home</a>
              </li>
              <li class="nav-item">
                <a class="about-nav nav-link" href="/about">About</a>
              </li>
              <li class="nav-item dropdown">
                <a class="product-nav nav-link dropdown-toggle" href="/product" id="navbarDropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                <!--data-toggle="dropdown"-->
                  Product
                </a>
                <div class="dropdown-menu" id="drop-down" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item door-nav" href="/product/pintu">Kaka Door</a>
                  <a class="dropdown-item kusen-nav" href="/product/kusen">Kaka Kusen</a>
                </div>
              </li>
              <li class="nav-item show-md-only">
                <a class="product-nav nav-link" href="/product">Product</a>
              </li>
              <li class="nav-item show-md-only">
                <a class="pintu-nav nav-link" href="/product/pintu">Kaka Door</a>
              </li>
              <li class="nav-item show-md-only">
                <a class="kusen-nav nav-link" href="/product/kusen">Kaka Kusen</a>
              </li>
              <li class="nav-item">
                <a class="contact-nav nav-link" href="/contact">Contact</a>
              </li>
            </ul>
          </div>
        </nav>


            <div class="content">
                @yield('content')
            </div>

            <!-- Footer -->
            <div class="footer">
                <div class="footer-contact">
                    <div class="hubungi">
                        <h4>HUBUNGI KAMI</h4>
                        <div class="contact-container">
                            <div class="phone contact">
                                <div class="phone-image contact-image">
                                    <i class="material-icons-outlined lg-icon">phone</i>
                                </div>
                                <div class="phone-text contact-text">
                                    <p>+62 31 3971927</p>
                                </div>
                                <div class="phone-button contact-button">
                                    <a href="tel:+62313971927"class="button-contact button-phone">Call Us</a>
                                </div>
                            </div>
                            <div class="mail contact">
                                <div class="mail-image contact-image">
                                    <i class="material-icons-outlined lg-icon">email</i>
                                </div>
                                <div class="mail-text contact-text">
                                    <p>marketing@kakadoor.com</p>
                                </div>
                                <div class="mail-button contact-button">
                                    <a href="mailto:marketing@kakadoor.com" class="button-contact button-mail">Mail Us</a>
                                </div>
                            </div>
                            <div class="address contact">
                                <div class="address-image contact-image">
                                    <i class="material-icons-outlined lg-icon">location_on</i>                              
                                </div>
                                <div class="address-text contact-text">
                                    <p>Jl. Veteran I No. 26, Gresik 61123, Indonesia</p>
                                </div>
                                <div class="address-button contact-button">
                                    <a href="https://goo.gl/maps/rpt8jnaYHeChdab46" target="_blank" class="button-contact button-address">Go to Our Office</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="signage">
                    <p>2020 by KakaDoor</p>
                </div>
            </div>
        </div>
    </body>
</html>

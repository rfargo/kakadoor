<link rel="stylesheet" href="{{ asset('css/product.css') }}">
@extends('layout')

@section('title', 'Produk')

@section('content')

<div class="product-door">
	<div class="product-door-img">
		<img src="{{ asset('img/Preview/MD Series (Ecoskin Finished)/1. PETRA/9. PETRA Golden Oak - Copy.jpg') }}" alt="Pintu Kaka Petra Golden Oak">
	</div>
	<div class="product-door-exp">
		<div class="door-title">
			<h1>Kaka Door</h1>
		</div>
		<div class="door-exp">
			<p>
				Sebuah kreasi yang berkualitas untuk pasar multi segmen dengan harga yang kompetitif.
				<br>
				<br>
				KAKA Door menawarkan pintu yang berbeda dari produk serupa yang ada di pasaran baik dari penampilan, konstruksi maupun bahan.
				<br>
				<br>
				Dalam katalog ini kaka menyediakan berbagai contoh produk-produk kaka yaitu :
				<br>
				<br>
				Series KAKA Door terdiri dari :
				<ul>
				<li>SC Series (solid colour series)</li>	
				<li>NC series (natural colour series)</li>	
				<li>MD series (minimalis door series)</li>	
				</ul>
			</p>
		</div>
		<div class="button-container door-button">
			<a href="/product/pintu" class="button button-door">Read More</a>
		</div>
	</div>
</div>


<div class="product-kusen">
	<div class="product-kusen-exp">
		<div class="kusen-title">
			<h1>Kusen Kaka Door</h1>
		</div>
		<div class="kusen-exp">
			<p>
				Kusen kaka adalah pasangan serasi untuk pintu kaka. Terbuat 100% kayu rimba yang keras (hard wood) yang diambil dari hutan lestari, dan telah kering setelah melalui proses oven.
				<br>
				<br>
				Diproduksi dengan sistem penyambungan yang unik untuk memperindah atau mempermudah saat instalasi.
				<br>
				<br>
				Pemasangan dapat dilakukan setelah bangunan hampir selesai atau finish. Tersedia beberapa tipe sebagai pilihan sesuai dengan kondisi dan fungsi yang dikehendaki.
				<br>
				<br>
				Tipe-tipe kusen KAKA Door yang ditawarkan meliputi :
				<ul>
					<li>Classic Type</li>
					<li>Double Rebate</li>
					<li>Single Rebate</li>
				</ul>
			</p>
		</div>
		<div class="button-container kusen-button">
			<a href="/product/kusen" class="button button-kusen">Read More</a>
		</div>
	</div>

	<div class="product-kusen-img">
		<img src="{{ asset('img/t series_00004.png') }}" alt="Kaka Kusen Tipe Double Rebate">
	</div>

</div>

@endsection
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('css/about.css') }}">

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@extends('layout')

@section('title', 'About Us')

@section('content')

<div class="about-header">
	<h2>About KAKA Door</h2>
</div>
<div class="about-exp">
	<div class="exp-1">
		<p>	
			KAKA Door adalah perusahaan yang berkomitmen untuk memenuhi kebutuhan jangka panjang bagi pelanggan kami. Kecenderungan sosial berubah dari konsumsi biasa menjadi konsumsi kreatif, banyak orang yang mendapatkan dan melatih gaya hidup mereka sendiri.
			<br>
			<br>
			Gaya hidup yang mapan sepertinya memberikan jalan kepada ide-ide dan konsep baru dalam hidup. Kami yakin ini adalah dedikasi kami untuk menyediakan barang yang akan memenuhi kebutuhan pribadi manusia yang bermacam – macam dan mendukung impian mereka. 
			<br>
			<br>
			Untuk menanggapi kebutuhan seperti itu, KAKA Door meningkatkan aktivitas produksi dalam meluaskan jangkauan dalam memproduksi pintu. Dari pintu kayu solid, pintu Solid Engineering, sampai pada pintu Hollow yang harganya paling ekonomis. Kami mempunyai staff peneliti dan juga staff yang berpengalaman khususnya di bidang kayu dan pintu yang berkerjasama untuk mengembangkan produk terbaru yang paling efisien dan tahan lama.
			<br>
			<br>
			“ Keinginan pasar selalu berkembang, begitu juga dengan produk kami “
		</p>
	</div>
	<div class="exp-grey">
		<p>
			"Visi kami adalah menjadi perusahaan global yang mengerti dan mendukung komunitas lokal, Indonesia dan Internasional untuk meningkatkan kualitas hidup manusia"
		</p>
	</div>
	<div class="exp-2">
		<div class="exp-2-title">
			<p>Ramah Lingkungan</p>
		</div>
		<p>
			Perusahaan kami berorientasi untuk menambah bahan baku yang ramah lingkungan pada produk kami, dan kami menggunakan kayu sebagai sumber bahan baku mentah yang kami peroleh dari Sustainable Forest Management atau perkebunan kayu legal. Selain itu kami mempunyai sistem Chain of Costudy (COC) yang mampu melacak kayu dari sumbernya sampai pada produk jadi kami. Sertifikasi COC yang kami peroleh adalah sebagai berikut :
		</p>
		<ul>
			<li>Forest Stewardship Council Chain of Custody ( FSC COC ) dari SMARTWOOD</li>
			<li>Programme for the Endorsement of Forest Certification (PEFC COC) dari Stichting  Keuringsbureau Hout (SKH)</li>
			<li>Sistem Verifikasi Legalitas Kayu (COC SVLK ) dari SUCOFINDO</li>
		</ul>
	</div>
</div>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item carousel-item-1 active">
    </div>
    <div class="carousel-item carousel-item-2">
    </div>
    <div class="carousel-item carousel-item-3">

    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

@endsection
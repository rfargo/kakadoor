<link rel="stylesheet" href="{{ asset('css/kusen.css') }}">


@extends('layout')

@section('title', 'Kusen')

@section('content')
<div class="exp">
	<div class="exp-detail">
		<div class="exp-detail-title">
			<h1>Keunggulan Kusen <br> Kaka Door</h1>
		</div>
		<div class="exp-detail-exp">
			<p>
				- Single rebate & double rebate
				<br>
				- 100% finish product, dengan cat “Alco Prime”.
				<br>
				- Menggunakan tropical hardwood yang telah dioven.  
				<br>
				- Menggunakan treatment khusus dengan impregnation sehingga tahan terhadap gangguan rayap.
				<br>
				- Diproses menggunakan teknik finger joint & laminating sehingga menjadi kusen yang stabil.
				<br>
				<br>
				 Kusen kaka adalah pasangan serasi untuk pintu kaka. Terbuat 100% kayu rimba yang keras (hard wood) yang diambil dari hutan lestari, dan telah kering setelah melalui proses oven. Diproduksi dengan sistem penyambungan yang unik untuk memperindah atau mempermudah saat instalasi. Pemasangan dapat dilakukan setelah bangunan hampir selesai atau finish. Tersedia beberapa tipe sebagai pilihan sesuai dengan kondisi dan fungsi yang dikehendaki.
			</p>
		</div>
	</div>
	<div class="exp-img">
		<img src="{{ asset('img/Screenshot 2020-09-17 100744_00016.png') }}" alt="Desain Kusen Kaka">
	</div>
</div>

<div class="classic">
	<div class="classic-img">
		<img src="{{ asset('img/kusen classic_00004.png') }}" alt="Kusen Kaka Tipe Classic">		
	</div>
	<div class="classic-exp">
		<div class="classic-title">
			<h2 class="title-top">Classic Type</h2>
		</div>
		<div class="classic-detail">
			<p>Kelebihan Kusen KAKA Classic adalah Progress Pemasangan yang lebih familiar, kuat & kokoh.</p>
		</div>
	</div>
</div>


<div class="t">
	<div class="t-exp">
		<div class="t-title">
			<h2>Double Rebate</h2>
		</div>
		<div class="t-detail">
			<p>
				Kelebihan Double Rebate adalah:
				<br>
				<br>
				1. Double Rebate, memberikan kelebihan ruang sehingga bisa dipasang pintu kasa nyamuk ataupun untuk pintu connecting.
				<br>
				<br>
				2. Fleksibel sehingga bukaan pintu bisa diatur atau dirubah.
				<br>
				<br>
				3. Bisa dipasang setela tembok jadi & sudah dicat.
				<br>
				<br>
				4. Cocok untuk renovasi.
			</p>
		</div>
		<div class="t-size">
			<p class="t-size-title">Ukuran Double Rebate</p>
			<p class="t-size-exp">
				<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
				<br>
				<span class="tab">Tinggi (H)</span>  : 50 mm
				<br>
				<span class="tab">Tebal (T)</span>    : 120 mm, 140 mm
				<br>
				<br>
				<span class="tab bold">Warna</span>  : Mahogany  & Maple 
			</p>
		</div>
	</div>
	<div class="t-img">
		<img src="{{ asset('img/t series_00004.png') }}" alt="Kusen Kaka Tipe Double Rebate">		
	</div>
</div>


<div class="l">
	<div class="l-img">
		<img src="{{ asset('img/L type_00002.png') }}"alt="Kusen Kaka Tipe Single Rebate">		
	</div>
	<div class="l-exp">
		<div class="l-title">
			<h2>Single Rebate</h2>
		</div>
		<div class="l-detail">
			<p>
				Kelebihan Single Rebate adalah:
				<br> 
				<br> 
				1. Lebih elegan.
				<br> 
				<br> 
				2. Lebih kuat.
				<br> 
				<br> 
				3. Bisa dipasang setelah tembok jadi.
				<br> 
				<br> 
				4. Cocok untuk renovasi.
				<br> 
				<br> 
			</p>
		</div>
		<div class="l-size">
			<p class="l-size-title">Ukuran Single Rebate</p>
			<p class="l-size-exp">
				<span class="tab">Tinggi (H)</span>  : 50 mm
				<br>
				<span class="tab">Tebal (T)</span>    : 120 mm, 140 mm
				<br>
				<br>
				<span class="tab bold">Warna</span>  : Caramel & Walnut 
			</p>
		</div>
	</div>
</div>
@endsection
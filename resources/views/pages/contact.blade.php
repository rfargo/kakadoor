<link rel="stylesheet" href="{{ asset('css/contact.css') }}">

@extends('layout')

@section('title', 'Contact Us')


@section('content')

<div class="contact-us">
	<h1>HUBUNGI KAMI</h1>
	<div class="contact-form">
		<P>Contact Us</P>
	    <form action="/contact" method="post" id="contact">

        	{{ csrf_field() }}

	        <div class="form-1 form-container">
		        <input class="form-control" type="text" value="{{old('first_name')}}" name="first_name" placeholder="First Name*" required />
		        
		        <input class="form-control" type="text"  value="{{old('last_name')}}" name="last_name" placeholder="Last Name" />
		    </div>

		    <div class="form-2 form-container">
		        <input class="form-control" type="email"  value="{{old('email')}}" name="email" placeholder="Email*" required/>
		        
		        <input class="form-control" type="text"  value="{{old('phone_number')}}" name="phone_number" placeholder="Phone" />
		    </div>
	        <textarea class="form-control" name="message" id="" placeholder="Type your message here..." cols="30" rows="10" required>{{old('message')}}</textarea>

	        <button class="btn btn-success btn-block">Send</button>
    	</form>

	    @error('first_name')
		    <div class="error-msg">{{ $message }}</div>
		@enderror
	    @error('last_name')
	    	<div class="error-msg">{{ $message }}</div>
		@enderror
	    @error('email')
	    	<div class="error-msg">{{ $message }}</div>
		@enderror
	    @error('phone_number')
	    	<div class="error-msg">{{ $message }}</div>
		@enderror
	    @error('message')
	    	<div class="error-msg">{{ $message }}</div>
		@enderror

    </div>
</div>


<script>
$(document).ready(function(){
    $('#contact input').blur(function(){
        if(!$(this).val()){
            $(this).addClass("error");
        } else{
            $(this).removeClass("error");
        }
    });

    $('#contact textarea').blur(function(){
        if(!$(this).val()){
            $(this).addClass("error");
        } else{
            $(this).removeClass("error");
        }
    });
});
</script>

@endsection

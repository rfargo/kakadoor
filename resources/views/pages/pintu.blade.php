<link rel="stylesheet" href="{{ asset('css/pintu.css') }}">


@extends('layout')

@section('title', 'Pintu')

@section('content')
<div class="exp">
	<div class="exp-img">
		<img class="img-top" src="{{ asset('img/Screenshot 2020-09-16 102046_00025.png') }}" alt="Desain Pintu Kaka">
		<img class="img-bottom" src="{{ asset('img/Screenshot 2020-09-16 102701_00005.png') }}" alt="Desain Pintu Kaka">
	</div>
	<div class="exp-detail">
		<div class="exp-detail-title">
			<h1>Keunggulan Kaka Door</h1>
		</div>
		<div class="exp-detail-exp">
			<p>
				Produk Kaka menggunakan bahan 100% Kayu keras(Hard wood) yang sudah melalui proses pengeringan dan menggunakan konstruksi finger joint serta laminating, untuk menghasilkan pintu yang lebih stabil dengan muai susut yang sangat rendah.
				<br>
				<br>
				Untuk pintu interior, Kaka menggunakan finishing Solvent Based yang tidak polutan, Elastis, ramah lingkungan agar kualitas penampilannya tetap terjaga.
				<br>
				<br>
				Untuk pintu utama, Kaka menggunakan cat Alco Prime yang berbahan dasar air (water-based) sehingga lebih ramah lingkungan, tidak beracun dan tidak mudah mengelupas. Terutama untuk pemakaian luar ruangan yang terkena percikan air hujan atau sinar matahari.
				<br>
				<br>
				Rangka konstruksi dengan ukuran yang lebih besar, sangat fleksibel untuk pemotongan pintu sesuai dengan kebutuhan.
				<br>
				<br>
				Pintu dapat dipotong sengan scrub listrik :
				<br>
				- Maksimal 15 mm untuk samping kiri dan kanan
				<br>
				- Maksimal 50 mm untuk ambang atas dan bawah
				<br>
				<br>
				Pintu dan kusen Kaka mempunyai jarak (allowance) sebesar 3mm. antara kusen dan pintu tidak perlu di set ulang sehingga memudahkan pemasangan
			</p>
		</div>
	</div>
</div>

<div class="video">
	<video id="videoPlayer" controls="controls" autoplay muted loop>
 	<!-- MP4 Video -->
	 <source src="{{URL::asset('vid/3d_4.mp4')}}" type="video/mp4">
	</video>
</div>

<div class="sc">
	<div class="sc-title">
		<h2>SC SERIES</h2>
	</div>
	<div class="sc-exp">
		<p>SC Series adalah pintu klasik dengan pattern oak, yang mempunyai cita rasa elegan.</p>
	</div>
	<div class="sc-door">
		<div class="sc-door-container">
			<div class="sc-door-img">
				<img src="{{ asset('img/kaka door/sc-napo.png') }}" alt="Pintu Kaka SC Napoleon 2P">
			</div>
			<div>
				<div class="sc-door-title">
					<h3>Napoleon 2P</h3>	
				</div>
				<div class="sc-door-exp">
					<p>
						<span class="sc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Golden Oak 
					</p>
				</div>
			</div>
		</div>
		<div class="sc-door-container">
			<div class="sc-door-img">
				<img src="{{ asset('img/kaka door/sc-co4.png') }}"alt="Pintu Kaka SC Colonel 4P">
			</div>
			<div>
				<div class="sc-door-title">
					<h3>Colonial 4P</h3>	
				</div>
				<div class="sc-door-exp">
					<p>
						<span class="sc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Mahogany 
					</p>
				</div>
			</div>
		</div>
		<div class="sc-door-container">
			<div class="sc-door-img">
				<img src="{{ asset('img/kaka door/sc-co6.png') }}" alt="Pintu Kaka SC Colonel 6P">			
			</div>
			<div>
				<div class="sc-door-title">
					<h3>Colonial 6P</h3>	
				</div>
				<div class="sc-door-exp">
					<p>
						<span class="sc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Caramel 
					</p>
				</div>
			</div>
		</div>
		<div class="sc-door-container">
			<div class="sc-door-img">
				<img src="{{ asset('img/kaka door/sc-co3.png') }}" alt="Pintu Kaka SC Colonel 3P">			
			</div>
			<div>
				<div class="sc-door-title">
					<h3>Colonial 3P</h3>	
				</div>
				<div class="sc-door-exp">
					<p>
						<span class="sc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Mahogany 
					</p>
				</div>
			</div>
		</div>				
		<div class="sc-door-container">
			<div class="sc-door-img">
				<img src="{{ asset('img/kaka door/sc-co2.png') }}" alt="Pintu Kaka SC Colonel 2P">				
			</div>
			<div>
				<div class="sc-door-title">
					<h3>Colonial 2P</h3>	
				</div>
				<div class="sc-door-exp">
					<p>
						<span class="sc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Teak 
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="sc-img">
	<img src="{{ asset('img/kaka door/Screenshot 2020-09-16 141831.png') }}" alt="Detail Tipe Pintu SC Kaka">
</div>


<div class="nc">
	<div class="nc-title">
		<h2>NC SERIES</h2>
	</div>
	<div class="nc-exp">
		<p>NC (Natural Color) Series adalah pintu interior dengan selected grade naturalistik. Kaka Door menyediakan bahan dan motif kayu import dengan pengerjaan yang lebih presisi</p>
	</div>
	<div class="nc-door">
		<div class="nc-door-container">
			<div class="nc-door-img">
				<img src="{{ asset('img/kaka door/nc-nap.png') }}" alt="Pintu Kaka NC Napoleon 2P">			
			</div>
			<div>
				<div class="nc-door-title">
					<h3>Napoleon 2P<h3>	
				</div>
				<div class="nc-door-exp">
					<p>
						<span class="nc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab bold">Pattern</span>  : Sapeli 
					</p>
				</div>
			</div>
		</div>
		<div class="nc-door-container">
			<div class="nc-door-img">
				<img src="{{ asset('img/kaka door/nc-col4b.png') }}" alt="Pintu Kaka NC Colonel 4P.B">
			</div>
			<div>
				<div class="nc-door-title">
					<h3>Colonial 4P.B</h3>	
				</div>
				<div class="nc-door-exp">
					<p>
						<span class="nc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Cherry Natural 
						<br>
						<span class="tab bold">Pattern</span>  : Cherry 
					</p>
				</div>
			</div>
		</div>
		<div class="nc-door-container">
			<div class="nc-door-img">
				<img src="{{ asset('img/kaka door/nc-col6b.png') }}"alt="Pintu Kaka NC Colonel 6P.B">			
			</div>
			<div>
				<div class="nc-door-title">
					<h3>Colonial 6P.B</h3>	
				</div>
				<div class="nc-door-exp">
					<p>
						<span class="nc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Walnut Natural 
						<br>
						<span class="tab bold">Pattern</span>  : Walnut 
					</p>
				</div>
			</div>
		</div>
		<div class="nc-door-container">
			<div class="nc-door-img">
				<img src="{{ asset('img/kaka door/nc-oval.png') }}"alt="Pintu Kaka NC Oval">		
			</div>
			<div>
				<div class="nc-door-title">
					<h3>Oval</h3>	
				</div>
				<div class="nc-door-exp">
					<p>
						<span class="nc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Ash Natural 
						<br>
						<span class="tab bold">Pattern</span>  : Ash 
					</p>
				</div>
			</div>
		</div>				
		<div class="nc-door-container">
			<div class="nc-door-img">
				<img src="{{ asset('img/kaka door/nc-nap1.png') }}"alt="Pintu Kaka NC Napoleon 2P">			
			</div>
			<div>
				<div class="nc-door-title">
					<h3>Napoleon 2P</h3>	
				</div>
				<div class="nc-door-exp">
					<p>
						<span class="nc-door-exp-title">Ukuran:</span>
						<span class="tab">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab bold">Warna</span>  : Teak Natural 
						<br>
						<span class="tab bold">Pattern</span>  : Teak 
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="nc-img">
	<img src="{{ asset('img/kaka door/Screenshot 2020-09-16 141831.png') }}" alt="Detail Tipe Pintu NC Kaka">
</div>


<div class="md">
	<div class="md-title">
		<h2>MD SERIES</h2>
	</div>
	<div class="md-exp">
		<p>MD (Minimalis Door) Series dapat digunakan untuk pintu interior & pintu utama, dengan desain panel yang simpel, modern dan variatif. MD Series cocok digunakan untuk berbagai ruangan yang formal dan informal.</p>
	</div>
	<div class="md-door">
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-1.png') }}" alt="Pintu Kaka MD Tango">
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Tango</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : Teak 
					</p>
				</div>				
			</div>
		</div>
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-2.png') }}" alt="Pintu Kaka MD Fortis">
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Fortis</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : Mahogany 
					</p>
				</div>				
			</div>
		</div>
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-3.png') }}" alt="Pintu Kaka MD Petra">		
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Petra</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : Walnut 
					</p>
				</div>				
			</div>
		</div>
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-4.png') }}" alt="Pintu Kaka MD Lintel">		
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Lintel</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : Caramel 
					</p>
				</div>				
			</div>
		</div>
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-5.png') }}"alt="Pintu Kaka MD Ruby">
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Ruby</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : Natural Oak 
					</p>
				</div>				
			</div>
		</div>
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-6.png') }}"alt="Pintu Kaka MD Horison">		
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Horison</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : White Oak Perla 
					</p>
				</div>				
			</div>
		</div>
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-7.png') }}"alt="Pintu Kaka MD Rova">		
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Rova</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : Maple 
					</p>
				</div>				
			</div>
		</div>
		<div class="md-door-container">
			<div class="md-door-img">
				<img src="{{ asset('img/kaka door/md-8.png') }}"alt="Pintu Kaka MD Carita">		
			</div>
			<div class="md-door-writing">
				<div class="md-door-title">
					<h3>Carita</h3>	
				</div>
				<div class="md-door-exp">
					<p>
						<span class="md-door-exp-title">Ukuran:</span>
						<span class="tab-small">Lebar (W)</span>  : 720, 820, 920 mm
						<br>
						<span class="tab-small">Tinggi (H)</span>  : 2100 mm
						<br>
						<span class="tab-small">Tebal (T)</span>    : 36 mm
						<br>
						<span class="tab-small bold">Warna</span>  : Sapeli Natural 
						<br>
						<span class="tab-small bold">Pattern</span>  : White Oak Perla  
					</p>
				</div>				
			</div>
		</div>
	</div>
</div>
<div class="md-img">
	<img src="{{ asset('img/kaka door/Screenshot 2020-09-16 142001.png') }}" alt="Detail Tipe Pintu MD Kaka">
</div>

@endsection
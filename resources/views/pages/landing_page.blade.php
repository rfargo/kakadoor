<link rel="stylesheet" href="{{ asset('css/landing_page.css') }}">


@extends('layout')

@section('title', 'Home')

@section('content')
<div class="video">
	<video id="videoPlayer" controls="controls" autoplay muted loop>
 	<!-- MP4 Video -->
	 <source src="{{URL::asset('vid/kaka6-720.mp4')}}" type="video/mp4">
	</video>
</div>
<div class="about-container">
	<div class="about">
		<div class="about-left">
			<h2 class="about-left-title">About Kaka Door</h2>
			<div class="about-left-exp">
				<p>
					Sebuah kreasi yang berkualitas untuk pasar multi-segmen. Dengan harga yang kompetitif. KAKA Door menawarkan pintu yang berbeda dari produk serupa yang ada di pasaran baik dari penampilan, konstruksi, maupun bahan.
				</p>
			</div>
			<a href="/about" class="about-left-read-more">Read More</a>
		</div>
		<div class="about-image">
			<img src="{{ asset('img/Home/Annotation 2020-09-01 224626.png') }}" alt="Pintu cokelat dan kusen merah">
		</div>
		<div class="about-right">
			<h2 class="about-right-title">20 Years of Experience</h2>
			<div class="about-right-exp">
				<p>	
					Kami memperhatikan setiap detail yang kami lakukan dalam proses produksi seperti kiln dry, dressing, mengkategorikan warna untuk memperbuat bagian yang berbeda dari pintu, ketepatan di bagian perakitan, perhitungan toleransi dalam celah yang mampu memuai dan menyusut dan juga seberapa banyak lem yang digunakan.
				</p>
			</div>
			<a class="about-right-read-more" href="/about">Read More</a>
		</div>
	</div>
</div>
<div class="catalog-pintu">
	<div class="catalog-pintu-title">
		<h2>CATALOG PINTU SERIES KAKA</h2>
	</div>
	<div class="pintu-list">
		<div class="pintu-1 pintu">
			<div class="pintu-title">
				<h3>SC Series</h3>				
			</div>
			<p class="pintu-exp">SC Series adalah pintu klasik dengan pattern oak, yang mempunyai cita rasa elegan.</p>
		</div>
		<div class="pintu-2 pintu">
			<div class="pintu-title">
				<h3>NC Series</h3>				
			</div>
			<p class="pintu-exp">NC Series adalah pintu interior dengan selected grade naturalistik. Kaka Door menyediakan bahan dan motif kayu import dengan pengerjaan yang lebih presisi</p>
		</div>
		<div class="pintu-3 pintu">
			<div class="pintu-title">
				<h3>MD Series</h3>				
			</div>
			<p class="pintu-exp">MD Series dapat digunakan untuk pintu interior & pintu utama, dengan desain panel yang simpel, modern dan variatif. MD Series cocok digunakan untuk berbagai ruangan yang formal dan informal.</p>
		</div>
	</div>
	<a href="/product" class="catalog-pintu-read-more">Read More</a>
</div>
<div class="catalog-kusen">
	<div class="catalog-kusen-title">
		<h2>CATALOG KUSEN SERIES KAKA</h2>
	</div>
	<div class="kusen-top">
		<div class="kusen-1 kusen">
			<div class="kusen-pic">
				<img src="{{ asset('img/kusen classic_00004.png') }}" alt="Kusen Kaka Tipe Classic">
			</div>
			<div class="kusen-title">Classic Type</div>
		</div>
	</div>
	<div class="kusen-bottom">
		<div class="kusen-2 kusen">
			<div class="kusen-title">Double Rebate</div>
			<div class="kusen-pic">
				<img src="{{ asset('img/t series_00004.png') }}" alt="Kusen Kaka Tipe Double Rebate">
			</div>
		</div>
		<div class="kusen-3 kusen">
			<div class="kusen-title">Single Rebate</div>
			<div class="kusen-pic">
				<img src="{{ asset('img/L type_00002.png') }}" alt="Kusen Kaka Tipe Single Rebate">
			</div>
		</div>				
	</div>
	<a href="/product" class="catalog-kusen-read-more">Read More</a>
</div>

<div class="company-detail">
	<div class="company-detail-1">
		<div class="company-detail-title company-detail-title-1">
			<h3>Kebikakan Manajemen</h3>
		</div>
		<div class="company-detail-container company-detail-container-1">
			<div class="company-detail-img company-detail-img-1"></div>
			<div class="company-detail-exp company-detail-exp-1">
				<p>
					Kami selalu mendengarkan suara pelanggan dan menanggapinya dengan peka dan tepat.
					 <br>
					 <br>
					Kami ingin membagikan peningkatan kemakmuran yang terus menerus bagi pelanggan kami dengan cara membangun hubungan kerja yang paling bersahabat berlandaskan saling percaya dan bisa diandalkan.
					 <br>
					 <br>
					Kami mendukung suasana perusahaan yang ceria, bersemangat dan berwawasan luas, penuh inovasi, ambisi dan berjiwa muda.
				</p>
			</div>
		</div>
	</div>
	<div class="company-detail-2">
		<div class="company-detail-title company-detail-title-2">
			<h3>Mengapa Pintu Kusen Kaka?</h3>
		</div>
		<div class="company-detail-container company-detail-container-2">
			<div class="company-detail-exp company-detail-exp-2">
				<ul>
					<li>Konstruksi kokoh</li>
					<li>Bahan pilihan berkualitas tinggi</li>
					<li>Tenaga ahli yang terampil</li>
					<li>Cat tahan cuaca bersertifikasi Eropa</li>
					<li>Sudah terbukti dan dipercaya</li>
					<li>100% finish product, dengan cat "alco prime"</li>
					<li>Menggunakan treatment khusus dengan impregnation sehingga tahan terhadap gangguan rayap</li>
					<li>Diproses menggunakan teknik finger joint & laminating sehingga menjadi kusen yang stabil.</li>
				</ul>
			</div>
			<div class="company-detail-img company-detail-img-2"></div>
		</div>
	</div>
	<div class="company-detail-3">
		<div class="company-detail-title company-detail-title-3">
			<h3>Sertifikasi</h3>
		</div>
		<div class="company-detail-container company-detail-container-3">
			<div class="company-detail-img company-detail-img-3">
				<img src="{{ asset('img/Home/Screenshot (11251).png') }}" alt="Sertifikat Rainforest Alliance Kaka">
				<img src="{{ asset('img/Home/Screenshot (11253).png') }}" alt="Sertifikat SKH KOMO Product Kaka">
				<img src="{{ asset('img/Home/Screenshot (11278).png') }}" alt="Sertifikat Sucofindo Kaka">
			</div>
			<div class="company-detail-exp company-detail-exp-3">
				<p>		
					CoC (PEFC & FSC)
					<br>
					KOMO – SKH
					<br>
					SLVK by SUCOFINDO
					<br>
					<br>
					Direview oleh badan Eropa setiap 3 bulan Sesuai ketentuan bertujuan untuk mutu terbaik
				</p>				
			</div>
		</div>
	</div>
</div>

<div class="company-point">
	<div class="company-point-1 company-point-container">
		<div class="company-point-1-img company-point-img">
            <i class="material-icons-round xl-icon">arrow_upward</i>
		</div>
		<div class="company-point-1-title company-point-title">
			<h3>DIVERSIFIKASI DESAIN</h3>
		</div>
		<div class="company-point-1-exp company-point-exp">
			<p>
				Mulai dari pintu polos yang paling sederhana sampai pada pintu yang elegan yang dibuat dengan cermat, kami menawarkan kepada pelanggan kami berbagai pilihan untuk memenuhi berbagai macam kebutuhan desain.
			</p>
		</div>
	</div>
	<div class="company-point-2 company-point-container">
		<div class="company-point-2-img company-point-img">
            <i class="material-icons-round xl-icon">schedule</i>			
		</div>
		<div class="company-point-2-title company-point-title">
			<h3>PENGIRIMAN TEPAT WAKTU</h3>
		</div>
		<div class="company-point-2-exp company-point-exp">
			<p>
				Mengatur kemampuan produksi untuk memastikan bahwa pintu terkirim tepat waktu.
			</p>
		</div>		
	</div>
	<div class="company-point-3 company-point-container">
		<div class="company-point-3-img company-point-img">
            <i class="material-icons-outlined xl-icon">forum </i>			
		</div>
		<div class="company-point-3-title company-point-title">
			<h3>QUALITY MINDED</h3>
		</div>
		<div class="company-point-3-exp company-point-exp">
			<p>
				Penelitian yang terus berkembang kami lakukan untuk memastikan bahwa produk kami adalah produk dengan kualitas yang terbaik.
			</p>
		</div>
	</div>
</div>
<div class="portfolio">
	<div class="portfolio-title">
		<h2>PORTFOLIO</h2>
	</div>
	<div class="portfolio-grid">
	  <div><img src="{{ asset('img/portfolio/5c36d45498bf13fe18be3f99d665fc31.png') }}" alt="Logo Persegres Gresik"></div>
	  <div><img src="{{ asset('img/portfolio/1200px-Telkom_Indonesia_2013.svg.png') }}" alt="Logo Telkom Indonesia"></div>
	  <div><img src="{{ asset('img/portfolio/049800200_1534128587-Foto_Liputan6.com.jpg') }}" alt="Logo WIKA"></div>
	  <div><img src="{{ asset('img/portfolio/202007241333-main.cropped_1595573981.jpg') }}" alt="Logo Pertamina"></div>
	  <div><img src="{{ asset('img/portfolio/CitySquareLogo Grad no-bkgrd.webp') }}" alt="Logo CitySquare"></div>
	  <div><img src="{{ asset('img/portfolio/download (1).png') }}" alt="Logo Aston"></div>
	  <div><img src="{{ asset('img/portfolio/download.jpg') }}" alt="Logo Gunawangsa Group"></div>
	  <div><img src="{{ asset('img/portfolio/download.png') }}" alt="Logo Lippo Cikarang"></div>
	  <div><img src="{{ asset('img/portfolio/Green-Land-Surabaya-logo-main.jpg') }}" alt="Logo Green Land Surabaya"></div>
	  <div><img src="{{ asset('img/portfolio/harrislogonew.png') }}" alt="Logo Harris Hotels"></div>
	  <div><img src="{{ asset('img/portfolio/Hotel_Ibis_logo_2012.png') }}" alt="Logo Hotel Ibis"></div>
	  <div><img src="{{ asset('img/portfolio/icon-flat.png') }}" alt="Logo Icon Land"></div>
	  <div><img src="{{ asset('img/portfolio/Logo-Ciputra.png') }}" alt="Logo Ciputra"></div>
	  <div><img src="{{ asset('img/portfolio/logo-educity.png') }}" alt="Logo Educity"></div>
	  <div><img src="{{ asset('img/portfolio/Logo-ITS-1-300x185.png') }}" alt="Logo ITS Surabaya"></div>
	  <div><img src="{{ asset('img/portfolio/logo-lg.png') }}" alt="Logo Tallasa City"></div>
	  <div><img src="{{ asset('img/portfolio/LOGO-ROUND-LOGO.png') }}" alt="Logo Universitas Ciputra"></div>
	  <div><img src="{{ asset('img/portfolio/logo-royal-residence.png') }}" alt="Logo Royal Residence"></div>
	  <div><img src="{{ asset('img/portfolio/LOGO-THE-ALIMAR.jpg') }}" alt="Logo The Alimar"></div>
	  <div><img src="{{ asset('img/portfolio/logo-the-taman-dayu.png') }}" alt="Logo The Taman Dayu"></div>
	  <div><img src="{{ asset('img/portfolio/RS-PHC.jpg') }}" alt="Logo RS PHC"></div>
	  <div><img src="{{ asset('img/portfolio/RS-royal-surabaya.jpg') }}" alt="Logo RS Royal Surabaya"></div>
	  <div><img src="{{ asset('img/portfolio/santika.ai_.png') }}" alt="Logo Hotel Santika"></div>
	  <div><img src="{{ asset('img/portfolio/swiss-belhotel-rainforest.jpeg') }}" alt="Logo Swiss-Belhotel rainforest"></div>
	  <div><img src="{{ asset('img/portfolio/Tamansari-Prospero.jpg') }}" alt="Logo Tamansari Prospero"></div>
	  <div><img src="{{ asset('img/portfolio/the-square-hotel_fb.jpg') }}" alt="Logo The Square Hotel"></div>
	  <div><img src="{{ asset('img/portfolio/unnamed.jpg') }}" alt="Logo Apartemen Puncak Dharmahusada"></div>
	  <div><img src="{{ asset('img/portfolio/VASA-LOGO-1-jpg.jpg') }}" alt="Logo Vasa Hotel Surabaya"></div>
	</div>
</div>

<script type="text/javascript">
	
</script>
@endsection
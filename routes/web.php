<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/landing_page');
});

Route::get('/home', function () {
    return view('pages/landing_page');
});

Route::get('/about', function () {
    return view('pages/about');
});

Route::get('/product', function () {
    return view('pages/product');
});

Route::get('/contact', function () {
    return view('pages/contact');
});

Route::get('/product/pintu', function () {
    return view('pages/pintu');
});

Route::get('/product/kusen', function () {
    return view('pages/kusen');
});

Route::post('/contact', 'App\Http\Controllers\ContactController@save');

//Route::get('/phpfirebase_sdk','FirebaseController@index');